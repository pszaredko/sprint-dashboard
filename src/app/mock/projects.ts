import { SprintStatus } from '../models/enums';
import { Project } from '../models/Project';
export const projects: Project[] = [
    {
        id: 'd7893dee-e70e-4f81-94b1-7c446276b9ca',
        name: 'Project 1',
        sprints: [
            {
                id: 'd2893dee-e70e-4f81-94b1-7c446276b9be',
                name: 'Sprint 1',
                startDate: '2022-01-15T17:17:42.541Z',
                endDate: '2022-01-18T17:17:42.541Z',
                status: SprintStatus.IS_WAITING,
                isRelease: false,
                releaseDate: null,
                doneDate: null,
            }
        ]
    },
]