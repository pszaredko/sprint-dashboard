import { Sprint } from './Sprint';
//@ts-ignore
import * as uuid from 'uuid';
export class Project {
    id: string;
    name: string;
    sprints: Sprint[];
    constructor(project?: any){
        this.id = project?.id || uuid.v4();
        this.name = project?.name || '';
        this.sprints = project?.sprints || [];
    }
}