export enum SprintStatus {
    IS_WAITING,
    INPROGRESS,
    DONE,
    ARCHIVED
}