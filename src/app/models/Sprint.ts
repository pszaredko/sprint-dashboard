//@ts-ignore
import * as uuid from 'uuid';
import { SprintStatus } from './enums';
export class Sprint {
    id: string;
    name: string;
    startDate: Date | string;
    endDate: Date | string;
    status: SprintStatus;
    isRelease: boolean;
    releaseDate: Date | null;
    doneDate: Date | null;
    constructor(sprint?: any){
        this.id = sprint?.id || uuid.v4();
        this.name = sprint?.name || '';
        this.startDate = sprint?.startDate || new Date().toISOString();
        this.endDate = sprint?.endDate || new Date().toISOString();
        this.status = sprint?.status || SprintStatus.IS_WAITING;
        this.isRelease = sprint?.isRelease || false;
        this.releaseDate = sprint?.releaseDate || null;
        this.doneDate = sprint?.doneDate || null;
    }
}

