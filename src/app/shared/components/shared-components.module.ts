import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SprintBoxComponent } from './sprint-box/sprint-box.component';
import { SharedPipesModule } from 'src/app/shared/pipes/shared-pipes.module';
import { MatIconModule } from '@angular/material/icon';



@NgModule({
  declarations: [
    SprintBoxComponent
  ],
  exports: [
    SprintBoxComponent
  ],
  imports: [
    CommonModule,
    SharedPipesModule,
    MatIconModule
  ]
})
export class SharedComponentsModule { }
