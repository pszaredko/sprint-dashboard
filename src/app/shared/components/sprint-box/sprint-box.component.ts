import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SprintStatus } from 'src/app/models/enums';
import { Sprint } from 'src/app/models/Sprint';
@Component({
  selector: 'app-sprint-box',
  templateUrl: './sprint-box.component.html',
  styleUrls: ['./sprint-box.component.scss']
})
export class SprintBoxComponent implements OnInit {
  @Input() sprint: Sprint;
  @Output() editIsClick: EventEmitter<any> = new EventEmitter();
  @Output() deleteIsClick: EventEmitter<any> = new EventEmitter(); 
  @Input() id: number = 0;
  SprintStatus = SprintStatus;
  constructor(
    public dialog: MatDialog
  ) { 
    this.sprint = new Sprint();
  }

  ngOnInit(): void {
  }

  edit(): void {
    this.editIsClick.emit(true);
  }

  delete(): void {
    this.deleteIsClick.emit(true);
  }
  
}
