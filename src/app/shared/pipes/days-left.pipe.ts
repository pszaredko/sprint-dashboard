import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
  name: 'daysLeft'
})
export class DaysLeftPipe implements PipeTransform {

  transform(value: Date | string | undefined): number {
    const date = moment(value);
    const today = moment();
    return date.diff(today, 'days');
  }

}
