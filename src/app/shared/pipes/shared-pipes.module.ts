import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DaysLeftPipe } from './days-left.pipe';



@NgModule({
  declarations: [
    DaysLeftPipe
  ],
  exports: [
    DaysLeftPipe
  ],
  imports: [
    CommonModule
  ]
})
export class SharedPipesModule { }
