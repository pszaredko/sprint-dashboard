import { Project } from './../../models/Project';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-project',
  templateUrl: './project.dialog.html',
  styleUrls: ['./project.dialog.scss']
})
export class ProjectDialog implements OnInit {
  project: Project;
  projectForm: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<ProjectDialog>,
    @Inject(MAT_DIALOG_DATA) public data: {project: Project},

    private _formBuilder: FormBuilder
  ) { 
    this.project = this.data.project;
    this.projectForm = this._formBuilder.group({
      id: [this.project.id, null],
      name: [this.project.name, [Validators.required]],
      sprints: [this.project.sprints, null]
    })
  }

  ngOnInit(): void {
  }

  confirm(): void {
    if (this.projectForm.valid) {
      this.dialogRef.close(this.projectForm.value);
    }
  }

  cancel(): void {
    this.dialogRef.close(false);
  }
}
