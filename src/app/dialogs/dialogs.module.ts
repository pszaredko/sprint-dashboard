import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ProjectDialog } from './project/project.dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SprintDialog } from './sprint/sprint.dialog';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatOptionModule } from '@angular/material/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { ArchivedSprintsDialog } from './archived-sprints/archived-sprints.dialog';
import { SharedComponentsModule } from '../shared/components/shared-components.module';
import { ConfirmDialog } from './confirm/confirm.dialog';



@NgModule({
  declarations: [
    ProjectDialog,
    SprintDialog,
    ArchivedSprintsDialog,
    ConfirmDialog
  ],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatSelectModule,
    MatOptionModule,
    SharedComponentsModule
  ]
})
export class DialogsModule { }
