import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivedSprintsDialog } from './archived-sprints.dialog';

describe('ArchivedSprintsDialog', () => {
  let component: ArchivedSprintsDialog;
  let fixture: ComponentFixture<ArchivedSprintsDialog>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArchivedSprintsDialog ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivedSprintsDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
