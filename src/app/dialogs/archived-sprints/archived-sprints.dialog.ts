import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Sprint } from 'src/app/models/Sprint';
import { ProjectDialog } from '../project/project.dialog';

@Component({
  selector: 'app-archived-sprints',
  templateUrl: './archived-sprints.dialog.html',
  styleUrls: ['./archived-sprints.dialog.scss']
})
export class ArchivedSprintsDialog implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ArchivedSprintsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: {sprints: Sprint[]},
  ) { }

  ngOnInit(): void {
  }

}
