import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Sprint } from 'src/app/models/Sprint';
import { ProjectDialog } from '../project/project.dialog';
import { SprintStatus } from '../../models/enums';

@Component({
  selector: 'app-sprint',
  templateUrl: './sprint.dialog.html',
  styleUrls: ['./sprint.dialog.scss']
})
export class SprintDialog implements OnInit {
  sprint: Sprint;
  sprintForm: FormGroup;
  SprintStatus = SprintStatus;
  constructor(
    public dialogRef: MatDialogRef<ProjectDialog>,
    @Inject(MAT_DIALOG_DATA) public data: {sprint: Sprint},

    private _formBuilder: FormBuilder
  ) { 
    this.sprint = this.data.sprint;
    this.sprintForm = this._formBuilder.group({
      id: [this.sprint.id, null],
      name: [this.sprint.name, [Validators.required]],
      startDate: [this.sprint.startDate, [Validators.required]],
      endDate: [this.sprint.endDate, [Validators.required]],
      status: [{value: this.sprint.status, disabled: this.sprint.status === SprintStatus.ARCHIVED}, [Validators.required]],
      isRelease: [this.sprint.isRelease, null],
      releaseDate: [this.sprint.releaseDate, null],
      doneDate: [this.sprint.doneDate, null]
    })
  }

  ngOnInit(): void {
  }

  confirm(): void {
    if (this.sprintForm.valid) {
      this.dialogRef.close(this.sprintForm.value);
    }
  }

  cancel(): void {
    this.dialogRef.close(false);
  }

  changeRelease(): void {
    this.sprintForm.get('isRelease')?.value ? this.sprintForm.get('releaseDate')?.setValue(new Date().toISOString()) : null;
  }

  statusChange(value: any): void {
    (value === SprintStatus.DONE) ? this.sprintForm.get('doneDate')?.setValue(new Date().toISOString()) : null;
  }
}
