import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SprintDialog } from './sprint.dialog';

describe('SprintDialog', () => {
  let component: SprintDialog;
  let fixture: ComponentFixture<SprintDialog>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SprintDialog ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SprintDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
