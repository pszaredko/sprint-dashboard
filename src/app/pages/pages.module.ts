import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProjectsComponent } from './projects/projects.component';
import { SprintsComponent } from './sprints/sprints.component';
import { SharedPipesModule } from '../shared/pipes/shared-pipes.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { SharedComponentsModule } from '../shared/components/shared-components.module';



@NgModule({
  declarations: [
    DashboardComponent,
    ProjectsComponent,
    SprintsComponent
  ],
  imports: [
    CommonModule,
    SharedPipesModule,
    MatDialogModule,
    MatIconModule,
    SharedComponentsModule
  ]
})
export class PagesModule { }
