import { Component, OnInit } from '@angular/core';
import { Sprint } from '../../models/Sprint';
import { MatDialog } from '@angular/material/dialog';
import { ProjectDialog } from '../../dialogs/project/project.dialog';
import { Project } from '../../models/Project';
import { SprintDialog } from '../../dialogs/sprint/sprint.dialog';
import { SprintStatus } from '../../models/enums';
import { DataService } from '../../core/components/services/data.service';
import { ArchivedSprintsDialog } from '../../dialogs/archived-sprints/archived-sprints.dialog';
import { ConfirmDialog } from 'src/app/dialogs/confirm/confirm.dialog';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  private $destroy: Subject<any> = new Subject();
  projects: Project[] = [];
  projectsWithArchivedSprints: Project[] = [];
  SprintStatus = SprintStatus;
  constructor(
    public dialog: MatDialog,
    private _dataService: DataService
  ) {
    this._dataService.getProjects()
      .pipe(
        takeUntil(this.$destroy)
      )
      .subscribe((projects: Project[]) => {
        this.projects = projects;
        this.projectsWithArchivedSprints = this.projects.map(project => { return {...project, sprints: project.sprints.filter(sprint => sprint.status === SprintStatus.ARCHIVED)}})
      })
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.$destroy.next(true);
    this.$destroy.unsubscribe();
  }

  addProject(): void {
    this.dialog.open(ProjectDialog, {
      data: {
        project: new Project()
      }
    })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.projects.push(result);
          this._dataService.setProjects(this.projects);
        }
      });
  }

  editProject(project: Project, projectIndex: number): void {
    this.dialog.open(ProjectDialog, {
      data: {
        project: project
      }
    })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.projects[projectIndex] = result;
          this._dataService.setProjects(this.projects);
        }
      });
  }

  deleteProject(projectIndex: number): void {
    this.dialog.open(ConfirmDialog)
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.projects.splice(projectIndex, 1);
          this._dataService.setProjects(this.projects);
        }
      })
  }

  addSprint(projectIndex: number): void {
    this.dialog.open(SprintDialog, {
      data: {
        sprint: new Sprint()
      }
    })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.projects[projectIndex].sprints.push(result);
          this._dataService.setProjects(this.projects);
        }
      });
  }

  editSprint(sprint: Sprint, projectIndex: number, sprintIndex: number): void {
    this.dialog.open(SprintDialog, {
      data: {
        sprint: sprint,
      }
    })
      .afterClosed()
      .subscribe((result: Sprint) => {
        if (result) {
          this.projects[projectIndex].sprints[sprintIndex] = result.status === SprintStatus.DONE && result.isRelease ? {...result, status: SprintStatus.ARCHIVED} : result;
          this._dataService.setProjects(this.projects);
        }
      });
  }

  deleteSprint(projectIndex: number, sprintIndex: number): void {
    this.dialog.open(ConfirmDialog)
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.projects[projectIndex].sprints.splice(sprintIndex, 1);
          this._dataService.setProjects(this.projects);
        }
      })
  }

  showArchivedSprints(projectIndex: number): void {
    this.dialog.open(ArchivedSprintsDialog, {
      data: {
        sprints: this.projectsWithArchivedSprints[projectIndex].sprints
      },
      width: '400px'
    })
  }

}
