import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  title: string = '';
  constructor(
    private _router: Router
  ) { 
    this._router.events.subscribe((event: any) => {
      if (event instanceof NavigationStart) {
        this.title = event.url.split('/')[1];
      }
    })
  }

  ngOnInit(): void {
  }

}
