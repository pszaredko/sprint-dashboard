import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { projects } from 'src/app/mock/projects';
import { Project } from '../../../models/Project';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private $projects: BehaviorSubject<Project[]> = new BehaviorSubject<Project[]>(projects);

  constructor() { }

  getProjects(): Observable<Project[]> {
    return this.$projects;
  }

  setProjects(projects: Project[]): void {
    this.$projects.next(projects);
  }

}
