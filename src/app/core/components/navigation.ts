import { Navigation } from '../../models/Navigation';
export const navigation: Navigation[] = [
    {
        name: 'Dashboard',
        route: '/dashboard',
        icon: 'dashboard'
    },
    {
        name: 'Projects',
        route: '/projects',
        icon: 'dvr' 
    },
    {
        name: 'Sprints',
        route: '/sprints',
        icon: 'list'
    }
]  