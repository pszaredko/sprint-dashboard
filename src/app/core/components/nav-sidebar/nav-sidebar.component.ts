import { Component, OnInit } from '@angular/core';
import { Navigation } from '../../../models/Navigation';
import { navigation } from '../navigation';

@Component({
  selector: 'app-nav-sidebar',
  templateUrl: './nav-sidebar.component.html',
  styleUrls: ['./nav-sidebar.component.scss']
})
export class NavSidebarComponent implements OnInit {

  public navigation: Navigation[] = navigation; 

  constructor() { }

  ngOnInit(): void {
  }

}
